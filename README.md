# Server http

Servidor básico para compartir carpetas en la red. Este servidor se construyó con la librería Fyne y usando el lenguaje de programación Go para permitir la compatibilidad con los sistemas operativos: FreeBSD, Linux, MacOS, Windows y Android.

También puedes usar este programa sin interfaz gráfica usando los argumentos como el puerto en el que dar servicio y/o la ruta del directorio a compartir.


<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/server/-/raw/master/capturas/consola.png" width="40%">
  &nbsp;
  <img alt="Light" src="https://gitlab.com/RicardoValladares/server/-/raw/master/capturas/escritorio.png" width="30%">
  &nbsp;
  <img alt="Dark" src="https://gitlab.com/RicardoValladares/server/-/raw/master/capturas/mobil.png" width="20%">
</p>


#### Instalar desde el compilador Go:

```bash
go install gitlab.com/RicardoValladares/server@latest
```

#### Ejecutar con interfaz gráfica:

```bash
server
```

#### Ejecutar sin interfaz gráfica, especificando puerto y directorio:

```bash
server 8080 /home
```
ó
```bash
server /home 8080
```

#### Ejecutar sin interfaz gráfica, especificando el directorio y usando puerto por default:

```bash
server /home
```

#### Ejecutar sin interfaz gráfica, especificando el puerto y usando el directorio local por default:

```bash
server 8080
```

