package main

import (
	"fmt"
	"errors"
	"net/url"
	"net/http"
	"strings"
	"io"
	"os"
	"net"
	"context"
	"sync"
	"time"
	"strconv"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"fyne.io/fyne/v2/storage"
	"github.com/yeqown/go-qrcode/v2"
	"github.com/yeqown/go-qrcode/writer/terminal"
)


var(
	Id fyne.App
	boton2 *widget.Button	
	estado = binding.NewString()
	puerto = binding.NewString()
	entry1 *widget.Entry
	logs *widget.Entry
	hyelink *widget.Hyperlink 
	Local string
	service HttpSrv
	ctx context.Context
) 


func main() {
	if len(os.Args) > 1 {
		var stat os.FileInfo 
		var err error
		Local, _ = os.Getwd()	
		dir := ""
		port := "8080"		
		if len(os.Args) == 2 {
			dir = os.Args[1]
			stat, err = os.Stat(dir)
			if err != nil {
				_, err = strconv.Atoi(dir)
				if err != nil {
					fmt.Println("El argumento no es valido.")
					return
				} else {
					port = os.Args[1]
					dir = Local
					stat, err = os.Stat(dir)
					if err != nil {
						fmt.Println("La ruta de ejecution no es un directorio valido, especifique el directorio en el argumento.")
						return
					}
				}
			}
		} else if len(os.Args) == 3 {
			dir = os.Args[1]
			stat, err = os.Stat(dir)
			if err != nil {
				_, err = strconv.Atoi(dir)
				if err != nil {
					fmt.Println("El primer argumento no es un directorio, ni un puerto valido.")
					return
				} else {
					port = dir
					stat, err = os.Stat(os.Args[2])
					if err != nil {
						fmt.Println("El segundo argumento no es un directorio valido.")
						return
					} else {
						dir = os.Args[2]
					}
				}			
			} else {		 
				_, err = strconv.Atoi(os.Args[2])
				if err != nil {
					fmt.Println("El segundo argumento no es un puerto valido.")
					return
				} else {
					port = os.Args[2]
				}
			}
		} else {
			fmt.Println("Argumentos invalidos")
			return
		}
		if !stat.Mode().IsDir() {
			fmt.Println("La ruta no es un directorio valido: ",dir)
			return
		}
		Local = dir
		iport, err := strconv.Atoi(port)
		if err != nil {
			fmt.Println("No es un puerto valido: ",port)
			return
		}		
		service = *NewHttpSrv(iport)
		if !service.Start() {
			fmt.Println("No es un puerto valido: ",port)
			return
		}
		qrc, err := qrcode.New("http://"+getIPLocal()+":"+port+"/carpeta/")
		if err == nil {
			fmt.Println("http://"+getIPLocal()+":"+port+"/carpeta/")
			w := terminal.New()
			qrc.Save(w)
		}
		ctx = context.Background()
		service.Shutdown(ctx)
		
	} else {
		Id = app.NewWithID("gitlab.com.ricardovalladares.server")
		Id.Settings().SetTheme(theme.DarkTheme())
		win := Id.NewWindow("Servidor")
		win.Resize(fyne.Size{Width: 450, Height: 350})
		Local = Id.Storage().RootURI().Path()
		showFilePicker := func() {
			onChosen := func(Dir fyne.ListableURI, err error) {
				if err != nil {
					dialog.ShowError(err, win)
					return
				}
				if Dir == nil {
					return
				}
				EliminarResago()
				Files, err := Dir.List()
				if err != nil {
					dialog.ShowError(err, win)
					return
				}
				for c:=0; c<len(Files); c++{
					Direction, err := url.QueryUnescape(Files[c].Name())
					if err != nil {
						dialog.ShowError(errors.New("No se logro parsear la ruta"), win)
						return
					}
					Fragment := strings.Split(Direction, "/")
					Name := Fragment[len(Fragment)-1]	
					if strings.Index(Name, ".") != 0 && strings.Index(Name, ".") != -1 {
						InputStreaming, err := storage.Reader( Files[c] )
						if err != nil {
							dialog.ShowError(err, win)
							return
						}
						data, err := io.ReadAll(InputStreaming)
						if err!=nil{
							dialog.ShowError(err, win)
							return
						}
						FileLocal, err := storage.Child( Id.Storage().RootURI() , Name)
						if err != nil {
							dialog.ShowError(err, win)
							return
						}
						OutputStreaming, err := storage.Writer(FileLocal)
						if err != nil {
							dialog.ShowError(err, win)
							return
						}
						OutputStreaming.Write(data)
					}
				}
			}
			dialog.ShowFolderOpen(onChosen, win)
		}
		
		boton1 := widget.NewButtonWithIcon("Carpeta a compartir", theme.FolderOpenIcon(), showFilePicker)
		slabel1 := binding.NewString()
		slabel1.Set("IP:")
		label1 := widget.NewLabelWithData(slabel1)
		ip := binding.NewString()
		ip.Set(getIPLocal())
		label2 := widget.NewLabelWithData(ip)
		Group1 := container.NewGridWithColumns(2, label1, label2)
		
		slabel3 := binding.NewString()
		slabel3.Set("Puerto:")
		label3 := widget.NewLabelWithData(slabel3)
		puerto.Set("8080")
		entry1 = widget.NewEntryWithData(puerto)
		Group2 := container.NewGridWithColumns(2,label3, entry1)

		boton2 = widget.NewButtonWithIcon("Activar servidor", theme.MediaPlayIcon(), OnOff)
		estado.Set("Off")
		
		logs = widget.NewMultiLineEntry()
		logs.SetPlaceHolder("Logs...")
		logs.Disable()
		logs.SetText("")

		link, err := url.Parse("https://gitlab.com/RicardoValladares/server")
		if err == nil {
		    link.Scheme = "https"
			link.Host = "gitlab.com"
		}
		hyelink = widget.NewHyperlink("Abrir en el navegador", link)
		
		card := widget.NewCard("Servidor HTTP", "",container.New(layout.NewVBoxLayout(), boton1, Group1, Group2, boton2))
		win.SetContent(container.NewVBox(card,logs,hyelink))
		win.CenterOnScreen()
		win.ShowAndRun()
	}
}


func OnOff() {
	status, err := estado.Get()
	if err != nil {
		txt := logs.Text
		logs.SetText("No se logro parsear el estado."+"\n"+txt)
		return
	}
	
	if status=="Off"{
		port, err := puerto.Get()
		if err != nil {
			txt := logs.Text
			logs.SetText("No se logro obtener el puerto."+"\n"+txt)
			return
		}
		iport, err := strconv.Atoi(port)
		if err != nil {
			txt := logs.Text
			logs.SetText("No es un puerto valido: "+port+"\n"+txt)
			return
		}	
		
		service = *NewHttpSrv(iport)
		if !service.Start() {
			txt := logs.Text
			logs.SetText("No es un puerto valido: "+port+"\n"+txt)
			return
		}
		link, err := url.Parse("http://localhost:"+port)
		if err == nil {
		    link.Scheme = "http"
		    link.Host = "localhost:"+port
		}
		hyelink.SetURL(link)	

		txt := logs.Text
		logs.SetText("Servicio Iniciado.\n"+txt)

		estado.Set("On")
		boton2.SetText("Desactivar servidor")
		boton2.SetIcon(theme.MediaStopIcon())
		entry1.Disable()
		
	} else {
		ctx = context.Background()
		if !service.Shutdown(ctx) {
			txt := logs.Text
			logs.SetText("No se logro detener el servicio. \n"+txt)
			return
		}
		link, err := url.Parse("https://gitlab.com/RicardoValladares/server")
		if err == nil {
		    	link.Scheme = "https"
			link.Host = "gitlab.com"
		}
		hyelink.SetURL(link)

		txt := logs.Text
		logs.SetText("Servicio Detenido.\n"+txt)
				
		estado.Set("Off")
		boton2.SetText("Activar servidor")
		boton2.SetIcon(theme.MediaPlayIcon())
		entry1.Enable()
	}
}	


func EliminarResago(){
	exist, err := storage.Exists(Id.Storage().RootURI())
	if err==nil && exist {
		DirLocal, err := storage.ListerForURI(Id.Storage().RootURI())
		if err != nil {
			return
		}
		Files, err := DirLocal.List()
		if err != nil {
			return
		}
		for c:=0; c<len(Files); c++{
			Direction, err := url.QueryUnescape(Files[c].Name())
			if err != nil {
				return
			}
			Fragment := strings.Split(Direction, "/")
			Name := Fragment[len(Fragment)-1]
			os.Remove(Local+"/"+Name)
		}
	}
}


func getIP(r *http.Request) string {
	ips := r.Header.Get("X-Forwarded-For")
	splitIps := strings.Split(ips, ",")
	if len(splitIps) > 0 {
		netIP := net.ParseIP(splitIps[len(splitIps)-1])
		if netIP != nil {
			return netIP.String()
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}
	netIP := net.ParseIP(ip)
	if netIP != nil {
		ip := netIP.String()
		if ip == "::1" {
			return "127.0.0.1"
		}
		return ip
	}
	return ""
}

func getIPLocal() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "127.0.0.1"
	}
	defer conn.Close()
	localAddress := conn.LocalAddr().(*net.UDPAddr)
	return localAddress.IP.String()
}



type HttpSrv struct {
	Port			int
	server			*http.Server
	isStarted		bool
	mtx			*sync.Mutex
}
 
func NewHttpSrv(port int) *HttpSrv {
	return &HttpSrv{
		Port:      port,
		server:    nil,
		isStarted: false,
		mtx:       &sync.Mutex{},
	}
}

func (srv *HttpSrv) Start() (ok bool) {
	srv.mtx.Lock()
	defer srv.mtx.Unlock()
	if srv.isStarted {
		return false
	}
	srv.isStarted = true
	mux := http.NewServeMux()
	mux.Handle("/carpeta/", http.StripPrefix("/carpeta", (http.FileServer(http.Dir(Local)))))
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><head><script>window.location='carpeta';</script></head><body><pre><a href='carpeta'>carpeta</a></pre></body></html>")
		if len(os.Args) == 1 {
			txt := logs.Text
			logs.SetText(getIP(r)+"\n"+txt)
		}
	})
	addr := fmt.Sprintf(":%v", srv.Port)
	srv.server = &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	go func() {
		if erro := srv.server.ListenAndServe(); erro != nil {
			srv.isStarted = false
		}
	}()
	time.Sleep(10 * time.Millisecond)
	return srv.isStarted
}
 
func (m *HttpSrv) Shutdown(ctx context.Context) (ok bool) {
	m.mtx.Lock()
	defer m.mtx.Unlock()
	if !m.isStarted || m.server == nil {
		return !m.isStarted
	}
	stop := make(chan bool)
	go func() {
		m.server.Shutdown(ctx)
		stop <- true
	}()
	select {
		case <-ctx.Done():
			break
		case <-stop:
			break
	}
	return !m.isStarted
}