
run:
	go run main.go

dependens:
	#pacman -S android-sdk android-sdk-platform-tools
	apt-get install -y android-sdk 
	apt-get install -y google-android-ndk-installer
	go install fyne.io/fyne/v2/cmd/fyne@latest

android:
	fyne package -os android -appID gitlab.com.ricardovalladares.server
	#export ANDROID_NDK_HOME="/usr/lib/android-ndk/" && export ANDROID_HOME="/usr/lib/android-sdk/" && export PATH="${PATH}:${ANDROID_HOME}tools/:${ANDROID_HOME}platform-tools/" && ~/go/bin/fyne package -os android -appID gitlab.com.ricardovalladares.server
	#keytool -genkey -v -keystore gplay.keystore -alias gitlab.com.ricardovalladares.server -keyalg RSA -keysize 2048 -validity 9999
	#jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore gplay.keystore server.apk gitlab.com.ricardovalladares.server
	