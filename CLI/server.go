package main

import (
	"fmt"
	"net/http"
	"strings"
	"os"
	"net"
	"context"
	"sync"
	"time"
	"strconv"
	"github.com/yeqown/go-qrcode/v2"
	"github.com/yeqown/go-qrcode/writer/terminal"
	"os/exec"
)


var(
	Local string
	service HttpSrv
	ctx context.Context
) 


func main() {
	if len(os.Args) > 1 {
		var stat os.FileInfo 
		var err error
		Local, _ = os.Getwd()	
		dir := ""
		port := "8080"		
		if len(os.Args) == 2 {
			dir = os.Args[1]
			stat, err = os.Stat(dir)
			if err != nil {
				_, err = strconv.Atoi(dir)
				if err != nil {
					fmt.Println("El argumento no es valido.")
					return
				} else {
					port = os.Args[1]
					dir = Local
					stat, err = os.Stat(dir)
					if err != nil {
						fmt.Println("La ruta de ejecution no es un directorio valido, especifique el directorio en el argumento.")
						return
					}
				}
			}
		} else if len(os.Args) == 3 {
			dir = os.Args[1]
			stat, err = os.Stat(dir)
			if err != nil {
				_, err = strconv.Atoi(dir)
				if err != nil {
					fmt.Println("El primer argumento no es un directorio, ni un puerto valido.")
					return
				} else {
					port = dir
					stat, err = os.Stat(os.Args[2])
					if err != nil {
						fmt.Println("El segundo argumento no es un directorio valido.")
						return
					} else {
						dir = os.Args[2]
					}
				}			
			} else {		 
				_, err = strconv.Atoi(os.Args[2])
				if err != nil {
					fmt.Println("El segundo argumento no es un puerto valido.")
					return
				} else {
					port = os.Args[2]
				}
			}
		} else {
			fmt.Println("Argumentos invalidos")
			return
		}
		if !stat.Mode().IsDir() {
			fmt.Println("La ruta no es un directorio valido: ",dir)
			return
		}
		Local = dir
		iport, err := strconv.Atoi(port)
		if err != nil {
			fmt.Println("No es un puerto valido: ",port)
			return
		}		
		service = *NewHttpSrv(iport)
		if !service.Start() {
			fmt.Println("No es un puerto valido: ",port)
			return
		}
		qrc, err := qrcode.New("http://"+getIPLocal()+":"+port+"/carpeta/")
		if err == nil {
			fmt.Println("http://"+getIPLocal()+":"+port+"/carpeta/")
			w := terminal.New()
			qrc.Save(w)
		}
		ctx = context.Background()
		service.Shutdown(ctx)
		
	} else {

		cmd := exec.Command("am", "start", "--user", "0", "-n", "gitlab.com.ricardovalladares.server/org.golang.app.GoNativeActivity")
		err := cmd.Run()
		if err != nil {
			if exec.Command("termux-open-url", "https://gitlab.com/-/project/56747323/uploads/f1ab12645775163ef6aeb1ee7d18ef24/server.apk").Run() != nil {
				fmt.Println("No se puede ejecutar interfaz grafica, para usar la consola pon un puerto y/o directorio")
			}
		}

	}
}


func getIP(r *http.Request) string {
	ips := r.Header.Get("X-Forwarded-For")
	splitIps := strings.Split(ips, ",")
	if len(splitIps) > 0 {
		netIP := net.ParseIP(splitIps[len(splitIps)-1])
		if netIP != nil {
			return netIP.String()
		}
	}
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return ""
	}
	netIP := net.ParseIP(ip)
	if netIP != nil {
		ip := netIP.String()
		if ip == "::1" {
			return "127.0.0.1"
		}
		return ip
	}
	return ""
}

func getIPLocal() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return "127.0.0.1"
	}
	defer conn.Close()
	localAddress := conn.LocalAddr().(*net.UDPAddr)
	return localAddress.IP.String()
}



type HttpSrv struct {
	Port			int
	server			*http.Server
	isStarted		bool
	mtx			*sync.Mutex
}
 
func NewHttpSrv(port int) *HttpSrv {
	return &HttpSrv{
		Port:      port,
		server:    nil,
		isStarted: false,
		mtx:       &sync.Mutex{},
	}
}

func (srv *HttpSrv) Start() (ok bool) {
	srv.mtx.Lock()
	defer srv.mtx.Unlock()
	if srv.isStarted {
		return false
	}
	srv.isStarted = true
	mux := http.NewServeMux()
	mux.Handle("/carpeta/", http.StripPrefix("/carpeta", (http.FileServer(http.Dir(Local)))))
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><head><script>window.location='carpeta';</script></head><body><pre><a href='carpeta'>carpeta</a></pre></body></html>")
	})
	addr := fmt.Sprintf(":%v", srv.Port)
	srv.server = &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	go func() {
		if erro := srv.server.ListenAndServe(); erro != nil {
			srv.isStarted = false
		}
	}()
	time.Sleep(10 * time.Millisecond)
	return srv.isStarted
}
 
func (m *HttpSrv) Shutdown(ctx context.Context) (ok bool) {
	m.mtx.Lock()
	defer m.mtx.Unlock()
	if !m.isStarted || m.server == nil {
		return !m.isStarted
	}
	stop := make(chan bool)
	go func() {
		m.server.Shutdown(ctx)
		stop <- true
	}()
	select {
		case <-ctx.Done():
			break
		case <-stop:
			break
	}
	return !m.isStarted
}